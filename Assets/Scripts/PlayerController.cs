﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    public float velocity;
    public Rigidbody rb;
    public Animator anim;
    bool attacking;

	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        GetInput();
        Move();
	}

    void GetInput()
    {
        if(Input.GetMouseButtonDown(0))
        {
            //print("Attack");
            Attack();
        }

        if(Input.GetKey(KeyCode.A))
        {
            SetVelocity(-1f);
        } else if(Input.GetKeyUp(KeyCode.A))
        {
            SetVelocity(0f);
            anim.SetInteger("Condition", 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            SetVelocity(1f);
        }
        else if (Input.GetKeyUp(KeyCode.D))
        {
            SetVelocity(0f);
            anim.SetInteger("Condition", 0);
        }
    }

    void Move()
    {
        if(velocity == 0)
        {
           // anim.SetInteger("Condition", 0);
            return;
        } else
        {
            if(!attacking)
                anim.SetInteger("Condition", 1);
            rb.MovePosition(transform.position + (Vector3.right * velocity * moveSpeed * Time.deltaTime));
        }
        
    }

    void SetVelocity(float dir)
    {
        if(dir < 0)
        {
            transform.LookAt(transform.position + Vector3.left);
        }else if (dir > 0)
        {
            transform.LookAt(transform.position + Vector3.right);
        }
        velocity = dir;
    }

    void Attack()
    {
      
        anim.SetInteger("Condition", 2);
        StartCoroutine(AttackRoutine());
    }

    IEnumerator AttackRoutine()
    {
        attacking = true;
        yield return new WaitForSeconds(1f);
        attacking = false;
    }
}
